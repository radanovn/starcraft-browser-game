<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{

    protected $fillable = ['name', 'price', 'resourse', 'resourseMin', 'img'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function building()
    {
        return $this->hasMany(Building::class, 'id');
    }
}
