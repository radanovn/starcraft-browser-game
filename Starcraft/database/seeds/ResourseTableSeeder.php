<?php

use App\Resourse;
use Illuminate\Database\Seeder;

class ResourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mineral = new Resourse;
        $mineral->name = 'minerals';
        $mineral->img =  'minerals.png' ;
        $mineral->save();
        
        $gas = new Resourse;
        $gas->name =  'gas';
        $gas->img =  'gas.png' ;
        $gas->save();
    }
}
