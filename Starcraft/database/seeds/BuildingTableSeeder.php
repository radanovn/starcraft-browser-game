<?php

use App\Building;
use Illuminate\Database\Seeder;

class BuildingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gasRefinery = new Building;
        $gasRefinery->name = 'gas';
        $gasRefinery->price = '750 mineral';
        $gasRefinery->resourse = 'mineral';
        $gasRefinery->img = 'gas.jpg';
        $gasRefinery->resourseMin = 5;
        $gasRefinery->save();


        $mineralMine = new Building;
        $mineralMine->name = 'minerals';
        $mineralMine->price = '750 gas';
        $mineralMine->resourse = 'gas';
        $mineralMine->img =  'mine.jpg' ;
        $mineralMine->resourseMin = 5;
        $mineralMine->save();

    }
}
