<!DOCTYPE html>

<html lang="en">


<!-- begin::Head -->

<head>
    <base href="../../../">
    <meta charset="utf-8" />
    <title>Starcraft | Buildings</title>
    <meta name="description" content="Invoice example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Asap+Condensed:500">

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{ asset('assets/css/pages/invoices/invoice-1.css') }}" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->


</head>

<!-- end::Head -->


<!-- begin::Body -->

<body class="kt-page-content-white kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">



    <!-- begin:: Page -->
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-invoice-1">
                    <div class="kt-invoice__head" style="background-image: url(assets/media/bg/starcraft1.jpg);">

                        <div class="kt-footer kt-grid__item" id="kt_footer">
                            <div class="kt-container ">
                                <div class="kt-footer__wrapper">

                                    <div class="kt-footer__menu">
                                        <a href="/buildings" target="_blank" class="kt-link">Home</a>
                                        <a href="http://weband.bg" target="_blank" class="kt-link">Contact</a>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="kt-invoice__container">
                            <!--begin::Section-->
                            <div class="kt-section">
                                <div class="kt-section__content kt-section__content--border kt-section__content--fit">
                                    <ul class="kt-nav">




                                        <li class="kt-nav__section kt-nav__section--first">
                                            <span class="kt-nav__section-text">Resourses</span>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a class="kt-nav__link">
                                                <img src="{{ asset('images/icons/reserve.png') }}" width="20px" height="20px">
                                                &emsp; <span class="kt-nav__link-text">Reserve</span>
                                            </a>
                                        </li>
                                        @foreach($resourses as $resourse)

                                        <li class="kt-nav__item">

                                            <a class="kt-nav__link">
                                                <img src="{{ asset('images/icons/'.$resourse->img) }}" width="20px" height="20px">
                                                
                                                &emsp; <span class="kt-nav__link-text">{{$resourse->name}} {{ $resourse->pivot->ammount }} </span>
                                            </a>
                                        </li>

                                        @endforeach
                                        <li class="kt-nav__separator kt-nav__separator--fit">
                                        </li>

                                        <li class="kt-nav__custom">
                                            <img src="{{ asset('images/icons/logout.png') }}" width="20px" height="20px">
                                            &emsp;
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>




                            <!--end::Section-->
                            <h1 class="kt-invoice__title">Buildings</h1>
                            @foreach($buildings as $building)
                            <div class="kt-invoice__brand">
                                <div href="#" class="kt-invoice__logo">
                                    <h1>{{ $building -> name }}</h1>
                                    <a href="{{route('buildings.show', $building->id) }}"><img src="{{ asset('images/'.$building->img) }}" width="280px" height="180px"></a>
                                    <span class="kt-invoice__desc">
                                        <span>Price {{ $building -> Price }}</span>
                                        <span>Resourse per min {{ $building -> resourseMin }}</span>
                                    </span>
                                </div>
                            </div>
                            @endforeach
                            <div class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <h3 class="kt-invoice__subtitle">Did you know?</h3>

                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">Buildings in StarCraft are all classified as large type units and generally have a base armor of one, with the exception of:</span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__text">Anti-air towers (Terran Missile Turret, Protoss Photon Cannon, Zerg Spore Colony)</span>
                                    <span class="kt-invoice__text">building supporters (Protoss Pylon and Zerg Creep Colony) - 0 armor</span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__text">Zerg Sunken Colony - 2 Armor
                                    </span>
                                </div>
                            </div>
                            <!-- begin:: Footer -->
                            <div class="kt-footer__copyright">
                                2020&nbsp;&copy;&nbsp;<a href="http://weband.bg" target="_blank" class="kt-link">Weband</a>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

</body>



<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
<script src="assets/js/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->
</body>

<!-- end::Body -->

</html>